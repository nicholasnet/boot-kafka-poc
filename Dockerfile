FROM openjdk:11 as build
WORKDIR /workspace/app
COPY . /workspace/app
RUN ./gradlew clean build jacocoTestReport && cp build/libs/*.jar /workspace/app/app.jar

FROM openjdk:11-jre
ARG user=appuser
ARG group=appuser
ARG uid=1000
ARG gid=1000

WORKDIR /workspace/app

RUN addgroup --system --gid ${gid} ${group} && \
    adduser --system --uid ${uid} --group ${group} && \
    chown -R ${user}:${group} /opt && \
    ln -snf /usr/share/zoneinfo/UTC /etc/localtime && \
    echo UTC > /etc/timezone

USER ${user}:${group}

COPY --from=build /workspace/app/app.jar ./

ENTRYPOINT ["java","-jar","app.jar"]
