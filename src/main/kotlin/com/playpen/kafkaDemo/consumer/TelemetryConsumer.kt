package com.playpen.kafkaDemo.consumer

import com.playpen.kafkaDemo.entity.Telemetry
import org.springframework.kafka.annotation.KafkaListener
import org.springframework.stereotype.Component

@Component
class TelemetryConsumer {

    @KafkaListener(topics = ["#{topic}"], groupId = "#{consumerGroupId}")
    fun consumer(telemetry: Telemetry) {

        println("Record fetched with ID ${telemetry.getId()} with Client ID ${telemetry.getClientId()} with speed ${telemetry.getSpeed()}")
    }
}
