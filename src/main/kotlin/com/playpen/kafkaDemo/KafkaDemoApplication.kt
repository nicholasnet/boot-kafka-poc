package com.playpen.kafkaDemo

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.ConfigurationPropertiesScan
import org.springframework.boot.runApplication

@SpringBootApplication
@ConfigurationPropertiesScan
class KafkaDemoApplication

fun main(args: Array<String>) {

	runApplication<KafkaDemoApplication>(*args)
}
