package com.playpen.kafkaDemo.publisher

import com.playpen.kafkaDemo.entity.Telemetry
import org.slf4j.LoggerFactory
import org.springframework.kafka.core.KafkaTemplate
import org.springframework.stereotype.Component
import java.time.Instant

@Component
class TelemetryPublisher(private val kafkaTemplate: KafkaTemplate<String, Telemetry>, private val topic: String) {

    private val logger = LoggerFactory.getLogger(TelemetryPublisher::class.java)

    fun publish(telemetry: Telemetry) {

        kafkaTemplate.send(topic, telemetry.getId(), telemetry).addCallback({

            val formattedDatetimeInUTCTimezone = if (it == null) "NOT FOUND" else Instant.ofEpochMilli(it.recordMetadata.timestamp()).toString()

            println("Record pushed at $formattedDatetimeInUTCTimezone with Id ${it?.producerRecord?.value()?.getId()}")

        }, {

            logger.error("Failed to publish record with ID ${telemetry.getId()} with Exception ${it.message}", it)
        })
    }
}
