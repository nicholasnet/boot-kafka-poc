import java.time.LocalDateTime
import java.time.ZoneOffset

object DateUtil {

    fun now(): LocalDateTime = LocalDateTime.now(ZoneOffset.UTC)
}
