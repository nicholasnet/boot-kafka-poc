package com.playpen.kafkaDemo

import io.confluent.kafka.schemaregistry.client.MockSchemaRegistryClient

object MockSchemaRegistry {

    val client = MockSchemaRegistryClient()
}
