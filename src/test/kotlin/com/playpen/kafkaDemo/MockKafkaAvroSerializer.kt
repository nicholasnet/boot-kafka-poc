package com.playpen.kafkaDemo

import io.confluent.kafka.serializers.KafkaAvroSerializer

class MockKafkaAvroSerializer : KafkaAvroSerializer(MockSchemaRegistry.client)
