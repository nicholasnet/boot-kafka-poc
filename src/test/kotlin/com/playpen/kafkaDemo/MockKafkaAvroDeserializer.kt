package com.playpen.kafkaDemo

import io.confluent.kafka.serializers.KafkaAvroDeserializer

class MockKafkaAvroDeserializer : KafkaAvroDeserializer(MockSchemaRegistry.client)
