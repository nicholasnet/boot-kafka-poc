package com.playpen.kafkaDemo.publisher

import com.playpen.kafkaDemo.MockKafkaAvroDeserializer
import com.playpen.kafkaDemo.entity.Telemetry
import io.kotest.matchers.shouldBe
import org.apache.avro.generic.GenericData
import org.apache.kafka.clients.consumer.Consumer
import org.apache.kafka.common.serialization.StringDeserializer
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.kafka.core.DefaultKafkaConsumerFactory
import org.springframework.kafka.test.EmbeddedKafkaBroker
import org.springframework.kafka.test.context.EmbeddedKafka
import org.springframework.kafka.test.utils.KafkaTestUtils
import org.springframework.test.annotation.DirtiesContext
import org.springframework.test.context.ActiveProfiles
import java.util.*

@SpringBootTest
@DirtiesContext
@EmbeddedKafka(topics = ["car-events"], partitions = 1)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ActiveProfiles("test")
internal class TelemetryPublisherIntegrationTest {

    @Autowired lateinit var telemetryPublisher: TelemetryPublisher

    @Autowired lateinit var embeddedKafkaBroker: EmbeddedKafkaBroker

    lateinit var consumer: Consumer<String, Any>

    @BeforeEach
    fun setup() {

        val configs = HashMap(KafkaTestUtils.consumerProps("group1", "true", embeddedKafkaBroker))
        configs["properties.specific.avro.reader"] = true

        consumer = DefaultKafkaConsumerFactory(configs, StringDeserializer(), MockKafkaAvroDeserializer()).createConsumer()
        embeddedKafkaBroker.consumeFromAllEmbeddedTopics(consumer)
    }

    @AfterEach
    fun tearDown() {

        consumer.close()
    }

    @Test
    fun `Can publish message in Kafka`() {

        val recordId = UUID.randomUUID().toString()

        telemetryPublisher.publish(Telemetry(recordId,5,5, DateUtil.now()))

        val consumerRecord = KafkaTestUtils.getSingleRecord(consumer, "car-events").value() as GenericData.Record

        consumerRecord.get("id").toString().shouldBe(recordId)
        consumerRecord.get("clientId").shouldBe(5)
    }
}
